#!/usr/bin/env python3

########################################################################################################################################
#
# This program inserts a fake SP with a given SNR, DM, width and scattering tail into existing Filterbank data
#
# Author:  Leon Houben
# Website: https://gitlab.com/houben.ljm
#
########################################################################################################################################

# Import modules
import os,sys
import argparse
import warnings
import matplotlib.pyplot as plt
import numpy as np
import copy
from scipy import signal
from scipy.optimize import curve_fit
from scipy.ndimage import rotate
from subprocess import Popen, PIPE
from presto import filterbank
import jdutil


class SP_Profile(object):
    """
    Make a fake single pulse profile over a range of frequencies.
    The profiles can be smeared, scattered and dispersed. 
    """
    def __init__(self,freqs,profile=False,Tprof=0.002,Fprof=False,dt=54.613333e-6,dm=0,amp2snr=False,abssnr=False,verbose=True):
        """
        SP Profile constructor

        Inputs:
            freqs:   Observing frequencies for each channel (in MHz).
            dt:      Sample time (in seconds). (Default: 54.613333e-6 s)
            dm:      Dispersion measure (in pc/cm^3). (Default: 0)
            profile: file name (*.npy) -> To use a custom numpy array as pulse profile
            Tprof:   1 float value -> Width of desired SP (in seconds). (Default: 0.002)
                     or list of [ctr0,amp0,fwhm0, ... , ctrN,ampN,fwhmN] -> Create a burst consisting of multiple subcomponents.
            Fprof:   fwhm in MHz of the desired Gaussian shape in the freq domain of the SP
                     or list of [ctr0,fwhm0, ... , ctrN,fwhmN] that specifies the shape of each burst subcomponent in the freq domain.
            amp2snr: Given amplitudes in Tprof are in units of SNR, so do not apply any amplitude scaling.
            abssnr:  Given snrs in Tprof are absolute SNRs. I.e. the bursts should end up with this SNR in the compined TS.
            verbose: To print additional operation info. (Default: True)

        Output:
            SP_Profile object
        """
        # Make larger frequency array assuming the first channel has the highest freq!
        self.dt = dt
        self.df = freqs[1]-freqs[0]
        self.dm = dm
        self.freqs   = freqs
        self.scat_dm = dm
        self.amp2snr = amp2snr
        self.abssnr  = abssnr
        self.verbose = verbose

        if profile:
            if not os.path.isfile(profile):
                raise IOError("%s does not exist!" % profile)

            custom_prof = np.load(profile)
            assert len(freqs) == custom_prof.shape[0], "Number of given frequencies do not agree with N-channels of loaded profile!"
            # Normalise custom SP profile
            self.numchans, self.numspectra = custom_prof.shape
            self.NProfile = custom_prof / (np.sum(custom_prof) / self.numchans) # Ensure the burst's profile has an area of numchans
        else:
            # Set the profile's time domain parameters
            if type(Tprof) == list:
                Tparams = np.array(Tprof,dtype=float).reshape(len(Tprof)//3,3)
            else:
                Tparams = np.array([[0.0,1.0,Tprof]])

            Tparams[:,0] /= dt #convert seconds to bins
            Tparams[:,2] /= dt
            self.Tprof = Tparams

            # Set the profile's frequency domain parameters
            self.nchan = self.freqs.shape[0]
            freq_fac = 2 # Factor to increase the frequency range with
            flow  = self.freqs[0] + np.flip(-1*self.df*np.arange(1,freq_fac*self.nchan+1))
            fhigh = self.freqs[-1] + self.df*np.arange(1,freq_fac*self.nchan+1)
            self.freqs = np.concatenate((flow,self.freqs,fhigh))
            np.clip(self.freqs, 1, None, self.freqs) #NOTE: clipping freqs here to prevent the occurance of negatieve freq values!
            self.Flimits = (freq_fac*self.nchan,freq_fac*self.nchan+self.nchan)
            if Fprof:
                if type(Fprof) == list:
                    Fparams = np.array(Fprof,dtype=float).reshape(len(Fprof)//2,2)
                else:
                    Fparams = np.array([self.freqs[round(len(self.freqs)/2)],Fprof])

                self.Fprof = Fparams

            else:
                self.Fprof = False

    def pulse_stack(self):
        self.Pstack = []
        for _ in range(0,self.Tprof.shape[0]):
           subburst = [np.ones_like(self.times, dtype=float),np.ones_like(self.freqs, dtype=float)]
           self.Pstack.append(subburst)
        
        return

    def multiGaus(self,x,*params):
        #params format assumed to be [ctr0,amp0,fwhm0, ... , ctrN,ampN,fwhmN]
        y = np.zeros_like(x)
        for idx in range(0,len(params),3):
            ctr  = params[idx]
            amp  = params[idx+1]
            fwhm = params[idx+2]
            y = y + amp * np.exp( -(4.*np.log(2)*(x - ctr)**2) / fwhm**2)
        return y

    def multiGaus2D(self,x,y,Xparams,Yparams):
        """
        This function assums the following parameter format:
            x       = numpy array
            y       = numpy array
            Xparams = [[ctr1,amp1,fwhm1], ... ,[ctrN,ampN,fwhmN]]
            Yparams = [[ctr1,fwhm1], ... ,[ctrN,fwhmN]]
        """
        if type(Xparams) == list:
            Xparams = np.array(Xparams)
        if type(Yparams) == list:
            Yparams = np.array(Yparams)

        if len(Xparams.shape) != 2 or len(Yparams.shape) != 2:
            raise IndexError("The Xparams and Yparams must be 2D arrays to create a 2D Gaussian!")
        elif Xparams.shape[0] != Yparams.shape[0]:
            if Yparams.shape[0] == 1:
                #Just a single Y structure is given copy to match Xparams
                Yparams = np.repeat(Yparams, Xparams.shape[0], axis=0)
            else:
                raise IndexError("The Xparams and Yparams must be of the same length to create a fully custom 2D multiGaus profile!")

        NP = np.zeros((len(x),len(y)))
        for idx,params in enumerate(Xparams):
            Xctr  = params[0]
            Xamp  = params[1]
            Xfwhm = params[2]

            Yctr  = Yparams[idx][0]
            Yfwhm = Yparams[idx][1]

            # Make 2D sub component
            prof = np.ones_like(NP)
            prof *= Xamp * np.exp( -(4.*np.log(2)*(x - Xctr)**2) / Xfwhm**2)
            prof *= np.transpose(np.exp( -(4.*np.log(2)*(y - Yctr)**2) / Yfwhm**2))[:,None]

            # Add sub component to total profile
            NP += prof

        return NP

    def find_peak(self, arr):
        peak = (np.diff(np.sign(np.round(np.diff(arr),10))) < 0).nonzero()[0] + 1
        if peak.shape[0] != 1:
            return np.NaN
        else:
            return peak[0]

    def build_burst(self,patchy=False,sp_idx=[0,None],sp_run=0,drift=False,tilt=False,cleave=False,save_prof=False):
        """
        Build a multi-component burst by adding seperatly created single pulses together

        Input:
            patchy:     True/False, wether to create a random burst 'blob' in frequency
            sp_idx:     [alpha0,nu_ref0, ... . alphaN,nu_refN] list of spectral index props alpha and nu_ref per subcomponent
            sp_run:     list of zeta per burst subcomponent
            drift:      {'drift_rate'} Drift rate in MHz / second to use to create a 'sad trombone' burst
                        {'cav_wths'} list of widths between burst components in seconds
            tilt:       angle or list of angles to rotate the subcomponents over
            cleave:     fwhm or list of [ctr0,fwhm0, ... ,ctrN,fwhmN] which specify where the pulse profile should be cleaved
                        with a Gaussian of width fwhm and at the position ctr relative to the centre of the profile
            save_prof:  Wether to save the compiled normalised pulse profile

        Output:
            Normalised multi-component pulse profile
        """
        if hasattr(self, 'NProfile'):
            # Normalised pulse profile already created!
            return

        ### Create time domain variablity
        # Create enough subcomponents if one wants to get a 'sad trombone' burst
        if drift:
            if "cav_wths" in drift and self.Tprof.shape[0] == 1:
                cav_wths = np.array(drift["cav_wths"], dtype=float) / self.dt # Convert to bins
                org_burst = self.Tprof[0]
                for cav in cav_wths:
                    new_burst = np.copy(org_burst)
                    new_burst[0] += cav
                    self.Tprof = np.vstack((self.Tprof, new_burst))
                    org_burst = new_burst
            elif "cav_wths" in drift and self.Tprof.shape[0] != (len(drift["cav_wths"]) + 1):
                raise IndexError("Their are not enough subcomponents for the number of cavities given!")
            elif self.Tprof.shape[0] == 1 and "drift_rate" in drift:
                raise ValueError("Subcomponents can not be created without specifying the cavity widths between the components!")
            else:
                # Ensure spacing between subcomponents is the same as the given cavity widths
                cav_wths = np.array(drift["cav_wths"], dtype=float) / self.dt # Convert to bins
                for num,cav in enumerate(cav_wths):
                    spacing = self.Tprof[num+1][0] - self.Tprof[num][0]
                    if not np.isclose(spacing,cav):
                        self.Tprof[num+1][0] = self.Tprof[num][0] + cav

        # Sort the subcomponents' amplitudes
        self.sort_params = np.argsort(self.Tprof[:,1])
        self.Tprof = self.Tprof[self.sort_params]

        # Get time dimensions in bins
        lo_extremity = np.min(self.Tprof[:,0]-self.Tprof[:,2]/2.)
        hi_extremity = np.max(self.Tprof[:,0]+self.Tprof[:,2]/2.)
        width = round(hi_extremity - lo_extremity) #in bins
        self.nspec = int(round(5*width))
        self.times = np.arange(-5.*width,5.*width)
        self.Tprof[:,0] += round(2*width-lo_extremity) # Reference all Gaussian centres to the centre of the profile window

        # Make a subcomponent stack
        self.pulse_stack()

        self.peak_limits = []
        center_freq = self.freqs[round(len(self.freqs)/2)]
        # Build the time domain shapes of subbursts
        for num,subburst in enumerate(self.Pstack):
            self.peak_limits.append([self.Tprof[num][0], center_freq]) #NOTE t in bins and f in MHz!!!
            subburst[0] *= self.multiGaus(self.times, 0,self.Tprof[num][1],self.Tprof[num][2])

        ### Apply frequency modulations
        # Create a emmission 'blob'
        if patchy and self.Fprof is False:
            self.patchy()

        # Apply custom frequency modulation
        if self.Fprof is not False:
            # Check if the time and freq domain parameters are compatible
            if len(self.Tprof.shape) != 2 or len(self.Fprof.shape) != 2:
                raise IndexError("The Tprof params and Fprof params must be 2D arrays to create a custom 2D multiGaus profile!")
            elif self.Tprof.shape[0] != self.Fprof.shape[0]:
                if self.Fprof.shape[0] == 1:
                    #Just a single Y structure is given copy to match Xparams
                    self.Fprof = np.repeat(self.Fprof, self.Tprof.shape[0], axis=0)
                else:
                    raise IndexError("The Tprof params and Fprof params must be of the same length to create a fully custom 2D multiGaus profile!")

            # Sort the subcomponents
            self.Fprof = self.Fprof[self.sort_params]

            # Build the frequency domain of subbursts
            for num,subburst in enumerate(self.Pstack):
                self.peak_limits[num][1] = self.Fprof[num][0]
                subburst[1] *= self.multiGaus(self.freqs,center_freq,1,self.Fprof[num][1])

        # Spectral index
        if sp_idx is not False:
            sp_idx = np.array(sp_idx)
            sp_idx = sp_idx.reshape(len(sp_idx)//2,2)
            if len(self.Pstack) > 1 and sp_idx.shape[0] == 1:
                sp_idx = np.repeat(sp_idx, len(self.Pstack), axis=0)
            elif len(self.Pstack) > 1 and sp_idx.shape[0] != len(self.Pstack):
                raise IndexError("The number of spectral index parameters must be the same as the number of subpulses!")

            sp_run = np.ones(len(self.Pstack)) * np.array(sp_run)

            # Sort the spectral parameters so the correspond to the proper sub-component
            sp_idx = sp_idx[self.sort_params]
            sp_run = sp_run[self.sort_params]

            for num,subburst in enumerate(self.Pstack):
                subburst[1] *= self.spectral_index(sp_idx[num][0],sp_idx[num][1],sp_run[num])
                peak = self.find_peak(subburst[1])
                if not np.isnan(peak):
                    peak_offset = center_freq - self.freqs[peak]
                    if peak_offset != 0:
                        # The peak freq has shifted translate it back to the centre
                        self.peak_limits[num][1] += peak_offset
                        subburst[1] = np.roll(subburst[1], round(len(self.freqs)/2)-peak)

        # Drifting sub pulses
        if drift is not False:
            self.drift(drift["drift_rate"], drift["cav_wths"])

        # Tilting sub pulses
        if tilt is not False:
            tilt = np.ones(len(self.Pstack)) * np.array(tilt, dtype=float)
            tilt = tilt[self.sort_params]

        ### Compile normalised pulse profile
        self.NProfile = np.zeros((self.nchan, self.nspec))
        for num,subburst in enumerate(self.Pstack):
            sub_burst  = np.ones((len(self.freqs), len(self.times)))
            sub_burst *= subburst[0]

            if self.amp2snr:
                A_old = np.sum(sub_burst)

            sub_burst *= subburst[1][:,None]

            if self.amp2snr:
                # Ensure the subburst's area remains the same before and after freq modulation
                sub_burst *= A_old/np.sum(sub_burst)

            if tilt is not False:
                sub_burst = rotate(sub_burst, tilt[num], reshape=False)

            # Get time domain limits
            Tlb = self.find_peak(subburst[0]) - self.nspec//2 + int(round(self.nspec/2 - self.peak_limits[num][0]))

            # Get freq domain limits
            Fpeak = self.find_peak(subburst[1])
            if np.isnan(Fpeak):
                Flb = self.Flimits[0]
            else:
                Flb = Fpeak - self.nchan//2 + int(round((center_freq - self.peak_limits[num][1])/self.df))

            sub_burst_sel = sub_burst[Flb:Flb+self.nchan,Tlb:Tlb+self.nspec]
            if self.amp2snr:
                TS_amp = np.max(np.sum(sub_burst_sel, axis=0))
                TS_amp_idx = np.argmax(np.sum(sub_burst_sel, axis=0))
                if self.abssnr:
                    sub_burst_sel *= (self.Tprof[num][1] - np.sum(self.NProfile, axis=0)[TS_amp_idx])/TS_amp
                else:
                    sub_burst_sel *= self.Tprof[num][1]/TS_amp

            self.NProfile += sub_burst_sel

        self.freqs = self.freqs[self.Flimits[0]:self.Flimits[1]]
        self.numchans, self.numspectra = self.NProfile.shape

        # Create cavities within the pulse profile
        if cleave is not False:
            self.cleave(cleave)

        if not self.amp2snr:
            self.NProfile = self.NProfile / (np.sum(self.NProfile) / self.numchans) # Make entire profile have a surface area of numchans

        ### Save compiled profile
        if save_prof is not False:
            self.save_profile(name=save_prof)

        return

    def spectral_index(self,alpha,nu_ref,zeta=0):
        """
        Scale the profile intensity as a function of frequency like:
        S = S_ref (nu / nu_ref)**(alpha + zeta * ln( nu / nu_ref ))
        """
        if nu_ref == None:
            nu_ref = self.freqs[0]

        freq_facs = (self.freqs/nu_ref)**(alpha + zeta*np.log(self.freqs/nu_ref))
        freq_facs *= len(freq_facs)/np.sum(freq_facs)

        return freq_facs

    def patchy(self):
        """
        Apply random Gaussian profile to freq domain of the subbursts,
        such that the dynamic spectrum shows a 'blob' of emmision.
        """
        bw      = self.freqs[self.Flimits[0]]-self.freqs[self.Flimits[1]]
        sp_ctrf = np.random.rand()*bw + self.freqs[self.Flimits[1]]
        sp_fwhm = np.random.uniform(0.,2.)*bw

        Fprofs = []
        for _ in range(self.Tprof.shape[0]):
            Fprofs.append([sp_ctrf,sp_fwhm]) # Add the random frequency modulation to each subburst

        # Make random blobby pulse profile
        self.Fprof = np.array(Fprofs)

    def drift(self, drift_rate, cav_wths):
        """
        Make a sad trombone shaped burst by moving the subburst in frequency
        according to the given drift rate.

        Inputs:
            drift_rate: How fast should subsequent subbursts drift down in freq [in MHz/s]
            cav_wths:   List of the seperation of subbursts in time in seconds
        """
        # Check if subbursts have a local maximum in frequency
        peaks = np.array([])
        for subburst in self.Pstack:
            peaks = np.append(peaks, self.find_peak(subburst[1]))

        if np.isnan(np.sum(peaks)):
            # Not all subbursts show a maximum in frequency
            # Make them all blobby to create a sad trombone burst
            Bbw  = (self.freqs[self.Flimits[0]] - self.freqs[self.Flimits[1]]) / (len(self.Pstack)+1)
            Bctr = self.freqs[self.Flimits[0]] - Bbw

            for num,subburst in enumerate(self.Pstack):
                self.peak_limits[num][1] = Bctr
                subburst[1] *= self.multiGaus(self.freqs,self.freqs[round(len(self.freqs)/2)],1,Bbw)

            if self.verbose:
                print("   Subburst have been made band limited, to make their freq drift visible")

        # Now create the drift, using the first subburst's centre frequency as reference freq
        cav_wths = np.array(cav_wths, dtype=float)
        Fdrift = cav_wths * float(drift_rate)
        for num in range(1,len(self.peak_limits)):
            self.peak_limits[num][1] = self.peak_limits[num-1][1] - Fdrift[num-1]

        return

    def cleave(self, cav_props):
        """
        Slice through a burst profile with anti-Gaussians; Ninja style!

        Inputs:
            cav_props:  fwhm in seconds of the desired Gaussian cut through the centre of the total pulse profile
                        or list of [ctr0,fwhm0, ... , ctrN,fwhmN] that specifies the shape of the Gaussian cuts
        """
        if type(cav_props) == list:
            cavs  = np.array(cav_props,dtype=float).reshape(len(cav_props)//2,2)
            cavs /= self.dt
            cavs  = np.insert(cavs, 1, 1., axis=1).flatten()
        else:
            cavs = np.array([0.,1.,cav_props/self.dt])

        # Get center of profile
        TS = np.sum(self.NProfile, axis=0)
        TS_para = self.estimate_pulse_para(TS=TS)

        x = np.arange(-1*self.numspectra//2, self.numspectra//2) - round(TS_para[0] - self.numspectra//2)
        y = self.multiGaus(x, *cavs)
        y = 1. - y

        self.NProfile *= y

    def estimate_pulse_para(self,TS=None,Weq=True,show_fit=False):
        """
        Find ctr, width and maximum of a rectangular pulse with the same
        area as the pulse to be injected.
        """
        # Initialise time series
        if type(TS) == type(None):
            TS = np.sum(self.NProfile, axis=0)
            init = True
        else:
            init = False

        # Estimate the rounding precision for determination pulse extremities
        max_val = TS.max()
        for decimal in range(0,100):
            if max_val > 5.:
                ndec = decimal
                break
            else:
                max_val *= 10.

        # Determine the crude boxcar parameters that fits the data
        A      = np.sum(TS)
        MAX    = np.max(TS)
        WTH_ex = np.where(TS.round(ndec) > 0)[0][-1] - np.where(TS.round(ndec) > 0)[0][0]
        CTR    = np.where(TS.round(ndec) > 0)[0][0] + WTH_ex/2
        WTH    = A/MAX #equivalent width

        if show_fit:
            x = np.arange(len(TS))
            plt.plot(x,TS)
            plt.plot(x,self.boxcar(x, CTR, MAX, WTH))
            plt.show()

        if init:
            self.ctr = CTR
            self.max = MAX
            self.width = WTH
        elif not Weq:
            return np.array([CTR,MAX,WTH_ex])
        else:
            return np.array([CTR,MAX,WTH])

    def boxcar_pulse_para(self,TS=None,show_fit=False):
        """
        Find ctr, width and maximum of the best boxcar convolution with TS
        """
        # Initialise time series
        if type(TS) == type(None):
            TS = np.sum(self.NProfile, axis=0)
            init = True
        else:
            init = False

        # Estimate the rounding precision for determination of boxcar widths
        max_val = TS.max()
        for decimal in range(0,100):
            if max_val > 5.:
                ndec = decimal
                break
            else:
                max_val *= 10.

        A_boxcars = []
        W_boxcars = np.arange(1, np.sum(TS.round(ndec)>0)+1, 1).astype(int)
        for boxcar in W_boxcars:
            conv = signal.convolve(TS, np.ones(boxcar), mode='valid') / np.sqrt(boxcar)
            A_boxcars.append(conv.max())

        WTH = np.argmax(A_boxcars)+1
        MAX = A_boxcars[WTH-1]/np.sqrt(WTH)
        CTR = signal.convolve(TS, np.ones(WTH), mode='valid').argmax() + WTH/2

        if show_fit:
            x = np.arange(len(TS))
            plt.plot(x,TS)
            plt.plot(x,self.boxcar(x, CTR, MAX, WTH),label="Best boxcar convolution")
            plt.plot(x,self.boxcar(x,*self.estimate_pulse_para(TS)),label="Best estimated boxcar")
            plt.legend()
            plt.show()

        if init:
            self.ctr = CTR
            self.max = MAX
            self.width = WTH
        else:
            return np.array([CTR,MAX,WTH])

    def propagate_burst(self,scint=False,smear=False,scatter=False,scat_dm=None):
        """
        Apply propagation effect to the created pulse profile

        Inputs:
            scint:      [Ns, scint_bw]
                        Ns, number of sinusoids used in scint pattern
                        The scintillation bw to use either:
                        - None, to use a random fraction of the total BW
                        - the scintillation bw in MHz to use
                        - 0, to create regular pattern with Ns sinusoids
            smear:      If true apply dm smearing
            scatter:    [scat_t, nu_ref]
                        scat_t, scattering time in seconds
                        nu_ref, referency freq to use in Mhz
            scat_dm:    dm to use for scattering
        """
        if self.amp2snr:
            TS_amp = np.max(np.sum(self.NProfile, axis=0))

        # Apply scintillation
        if scint is not False:
            self.scintillation(*scint)

        # Apply smearing
        if smear:
            self.smear_convolve()

        # Apply scattering
        if scatter != False:
            if scat_dm != None:
                self.scat_dm = scat_dm
            self.scatter(*scatter)

        if self.amp2snr:
            self.NProfile *= TS_amp/np.max(np.sum(self.NProfile, axis=0))

        return

    def boxcar(self,x,ctr,maxx,width):
        return maxx/2. * (np.sign(x-(ctr-width/2.)) - np.sign(x-(ctr+width/2.)))

    def rev_heaviside(self,x):
        y = np.ones_like(x)
        y[x > 0.0] = 0.0
        return y

    def get_max(self):
        """
        Get maximum value of the pulse profile
        """
        self.max_val = self.NProfile.max()

    def scale(self,frac):
        """
        Scale the profile to attain a given summed SNR of the SP
        """
        frac = np.ones(self.NProfile.shape[0]) * frac #To alow a scalar to be given
        self.NProfile *= frac[:,None] # Since area profile PER channel = 1!

    def smear_convolve(self): #Returns the DM smearing in seconds over a bandwidth BW (MHz) centered at a frequency nu (MHz)
        """
        Smears SP profile in a 2D numpy array with the DM smearing time
        in seconds over a bandwidth BW (MHz) centered at a frequency nu (MHz)
        """
        # Get area of pulse
        A_old = np.sum(self.NProfile)
        
        Tsmear = 8.3e3 * self.dm * (self.freqs[0]-self.freqs[1]) / (self.freqs**3) / self.dt #in bins

        if self.verbose:
            print("   Smearing with %0.6f seconds" % (Tsmear[int(len(self.freqs)/2)]*self.dt))

        # Append padding when necesarry
        if Tsmear[-1] > (self.numspectra/2):
            padding = np.zeros((self.numchans,int(Tsmear[-1]*1.05)))
            self.NProfile = np.append(self.NProfile,padding,axis=1)
            self.numspectra = len(self.NProfile[0])

        # Apply smearing to NProfile
        x    = np.arange(self.numspectra)
        x_ar = np.zeros((self.numchans,self.numspectra)) + x
        edge = np.round(Tsmear).astype('int').reshape(self.numchans,1)
        boxcar = self.rev_heaviside(x_ar-edge)
        boxcar = boxcar / np.sum(boxcar, axis=1)[:,None] # Normalised boxcars

        self.NProfile = np.fft.irfft(np.fft.rfft(self.NProfile)*np.fft.rfft(boxcar),self.numspectra,axis=1)

        # Normalise resulting convolution
        A_new = np.sum(self.NProfile)
        self.scale(A_old/A_new)

    def scatter(self,scat_t=None,nu_ref=None):
        """
        Scatter NProfile accoriding to the pulse broadening function
        by Cordes and Lazio 2003 (Default) or a nu^-4.4 relation with
        custom scatter time at top of the band.
        """
        # Get area of pulse
        A_old = np.sum(self.NProfile)

        if self.scat_dm <= 0:
            raise ValueError("Scattering will not be applied because DM is zero or negative")

        if scat_t != None:
            if nu_ref == None:
                nu_ref = self.freqs[0]
            else:
                nu_ref = float(nu_ref)
            tau_scatt = float(scat_t) * (self.freqs/nu_ref)**-4.4 / self.dt #in bins
        else: #NOTE: approximation with approximate error of 0.65 in log(tau)
            logDM = np.log10(self.scat_dm)
            tau_scatt = 10.0**(-3.59 + 0.129*logDM + 1.02*logDM**2. - 4.4*np.log10(self.freqs/1000.0))/1.0e6 / self.dt #in bins
    
        # Append padding when necesarry
        if (tau_scatt[-1]*7.) > (self.numspectra/2): # ~0.1% limit
            padding = np.zeros((self.numchans,int(tau_scatt[-1]*7.05)))
            self.NProfile = np.append(self.NProfile,padding,axis=1)
            self.numspectra = len(self.NProfile[0])

        # Apply scattering to NProfile
        if self.verbose:
            print("   Scattering with tau_scat = %0.6f seconds" % (tau_scatt[int(len(self.freqs)/2)]*self.dt))
        x = np.arange(self.numspectra)
        x_ar = np.zeros((self.numchans,self.numspectra)) + x
        tau_scatt = tau_scatt.reshape(self.numchans,1)
        tail = (1/tau_scatt) * np.exp(-x_ar/tau_scatt) #Normalised exponantial decay function

        self.NProfile = np.fft.irfft(np.fft.rfft(self.NProfile)*np.fft.rfft(tail),self.numspectra,axis=1)

        # Normalise resulting convolution
        A_new = np.sum(self.NProfile)
        self.scale(A_old/A_new)
    
    def scintillation(self,Ns=5,sc_bw=None):
        """
        Inputs:
            Ns:     Number of sinusoids created in the scintillation pattern.
            sc_bw:  Scintillation bandwidth, the minimum scintal bandwidth in MHz
                    If None defaults to 1% to 2% of the total bandwidth.
                    If 0, a regular scintillation pattern will be created with Ns peaks.

        Output:
            NProfile with an amplitude variation that follows a multi-sine curve.
        """
        # Get area of pulse
        A_old = np.sum(self.NProfile)

        chan_bw = (self.freqs[0] - self.freqs[1])

        regular = False
        sc_what = "random"
        if sc_bw is None:
            sc_bw = (np.random.rand() + 1) / 100. * self.numchans # 1-2% of total bandwidth
        elif sc_bw == 0:
            regular = True
            sc_what = "regular"
        else:
            sc_bw = float(sc_bw) / chan_bw # in number of channels

        FS   = np.sum(self.NProfile, axis=1)
        FS_w = np.sum(FS)/np.max(FS)
        x  = np.arange(FS_w).astype(float)
        x /= x.max() # Set range from 0 to 1

        y = np.zeros_like(x)
        if regular:
            y += np.sin(2*np.pi*Ns*x + np.random.rand()*2*np.pi)
        else:
            for num in range(Ns): # Use Ns sinusoids
                y += np.sin(2*np.pi*(FS_w/sc_bw)*x + np.random.rand()*2*np.pi)
                sc_bw *= (np.random.rand()+0.8)

        if self.verbose:
            print(f"   Applying a {sc_what} scintillation pattern with a scint BW of {int(sc_bw)} chans and build of from {Ns} sinusoids.")

        y = np.clip(y,0,None) # Remove negative values
        y /= np.max(y)

        # Centre scintillation pattern correctly over the pulse profile
        FS_ctr = FS.argmax()
        if FS_ctr < len(x)//2:
            FS_ctr = len(x)//2
        elif FS_ctr > (self.numchans-len(x)//2):
            FS_ctr = self.numchans-len(x)//2-1 # -1 to take into account int conversion errors

        left_patch = FS_ctr - len(x)//2
        y = np.append(np.zeros(left_patch),y)
        y = np.append(y,np.zeros(self.numchans-len(y)))

        #Apply the sinusoidal scintillation partern to profile
        self.scale(y)

        #Ensure profile keeps the same area to preserve correct snr scaling
        A_new = np.sum(self.NProfile)
        self.scale(A_old/A_new)

    def amplitude_scaling(self,chan_bw=None,rms=None,flux=None,snr=None):
        if not hasattr(self, "ctr") and not hasattr(self, "width") and not hasattr(self, "max"):
            # Determine profile parameters with propagation effects
            self.boxcar_pulse_para()

        ### Apply flux or SNR scaling
        # Specify default values
        if chan_bw == None:
            chan_bw = self.df
        if rms == None:
            rms = np.std(self.NProfile)

        if flux == None:  # 1. Using fixed SNR value
            SNR = snr
        else:             # 2. Using a flux and SEFD
            SNR = np.sqrt(2.*self.numchans*abs(chan_bw)*1.e6*self.width*self.dt)*flux[0]/flux[1]


        ######################################################################################################
        # In order to inject pulses which are not purely Gaussian shaped, convert the theoretical Gaussian 
        # to top_hat area conversion into a numerical method. This is done through ensuring that the  area 
        # under the FWHM of the pulse (the boxcar area) is equal to the total area (Gaussian area) of the pulse
        ######################################################################################################
        #A_fwhm = np.sum(self.NProfile[:,int(np.floor(self.ctr-self.width/2.)):int(np.floor(self.ctr+self.width/2.))])
        #A_frac = self.NProfile.shape[0] / A_fwhm
        #NOTE: do not use this method! Whenever the pulse shape deviates strongly from a Gaussian (for instance when a
        #      scattering tail is pressent) A_fwhm can become rather small making the scale fraction HUGE.

        if self.amp2snr:
            SNR = np.sum(self.NProfile)
            self.NProfile *= self.numchans / SNR # Scale profile to correct for partial bandwidth coverage
            scale_frac = np.ones_like(self.freqs)*SNR*rms / self.numchans
            SNR = np.sum(self.NProfile,axis=0).max() # SNR of brightest subcomponent!
        else:
            ######################################################################################################
            # Use SNR calculation for a SP from McLaughlin & Cordes, 2003, ApJ 596
            # But converted to apply to the surface area of a Gaussian through multiplication with sigma*sqrt(2*pi)
            # And using the timeseries SNR and RMS for which holds: SNR_ts = sqrt(N_chan) * SNR_chan
            ######################################################################################################
            #eta = (np.pi / (8*np.log(2)))**(1/4) # pulse-shape–dependent factor for a Gaussian shaped pulse
            #N_pol = 2. # Number of summed polarisations
            #scale_frac = SNR*rms*np.sqrt(self.width) / (eta * self.numchans * np.sqrt(N_pol * abs(self.df)))

            #NOTE: this method assumes pure Gaussian shaped injected pulses!

            """
            Apply an SNR scaling fully determined from data params and use McLaughlin & Cordes to calculate
            physical properties from these data params. This scaling method is inspired by the way the SNR
            scaling is applied by the injection software Furby.
            """
            boxcar_snr = self.max*np.sqrt(self.width) / rms
            scale_frac = SNR / boxcar_snr

        self.scale(scale_frac)
        self.get_max() # Set maximum value of profile for data scaling into dtype range

        if self.verbose:
            print("   Profile scaled to an SNR of %0.4f with a scale fraction of %0.5f" % (SNR,scale_frac))
            print("   Using an RMS of %0.4f" % rms)

        return SNR

    def disperse(self, dm=None, calc_only=False):
        """
        To calculate the time difference from a specific reference frequency, set nu1 to be that reference freq.
        For example set it to be the highest freq of the band.
        """
        if dm == None:
            dm = self.dm

        DisC = 4.1488e3          #Dispersion Constant
        Tdisperse = DisC * dm * (1.0/(self.freqs**2) - 1.0/(self.freqs[0]**2)) / self.dt #in bins

        if not calc_only:
            # Append padding when necesarry
            if Tdisperse[-1] > (self.numspectra/2):
                padding = np.zeros((self.numchans,int(Tdisperse[-1]*1.05)))
                self.NProfile = np.append(self.NProfile,padding,axis=1)
                self.numspectra = len(self.NProfile[0])

            if self.verbose:
                print("   Dispersing with %0.6f seconds" % (Tdisperse[int(len(self.freqs)/2)]*self.dt))

            # Apply despersion to NProfile
            for idx,TS in enumerate(self.NProfile):
                rollby = int(np.round(Tdisperse[idx]))
                self.NProfile[idx] = np.roll(TS,rollby)
        else:
            # Return duration dm_sweep
            return Tdisperse[-1]*self.dt

    def int_prof(self, nbits):
        """
        Convert profile to integer value profile preserving its area.
        """
        # Get the dtype extrimities
        dtype = np.iinfo(filterbank.get_dtype(nbits))

        A_orig = np.sum(self.NProfile)
        
        # If sample values to low, kick-start conversion
        if self.NProfile.max() < 0.5:
            self.NProfile *= 0.5/self.NProfile.max()

        # Find the number of element in NProfile that have a value 1 miljonth less than profile max
        # A value considered as the lowest value of the profile, i.e. 0 in the int profile.
        A_max = len(np.where(self.NProfile > self.NProfile.max()*1e-06)[0])*dtype.max

        A_old_ratio = np.sum(np.clip(np.round(self.NProfile), dtype.min, dtype.max)) / A_orig
        if A_old_ratio > 1.:
            scale_direction = "DOWN"
            ratio = 0.99
        else:
            scale_direction = "UP"
            ratio = 1.01

        if self.verbose:
            print("   Converting profile to integer values")

        while True:

            A_new = np.sum(np.clip(np.round(self.NProfile), dtype.min, dtype.max))
            A_new_ratio = A_new / A_orig
            A_max_ratio = A_new / A_max

            if (scale_direction == "UP") and (A_new_ratio > 1.):
                if (abs(1-A_old_ratio) < abs(1-A_new_ratio)):
                    # The old scalled prof had an A closer to the original, so scale it back
                    self.scale(0.99)
                    A_new_ratio = A_old_ratio # Print proper ratio of data below

                self.NProfile.round(out=self.NProfile)
                break
            elif (scale_direction == "DOWN") and (A_new_ratio < 1.):
                if (abs(1-A_old_ratio) < abs(1-A_new_ratio)):
                    # The old scalled prof had an A closer to the original, so scale it back
                    self.scale(1.01)
                    A_new_ratio = A_old_ratio # Print proper ratio of data below

                self.NProfile.round(out=self.NProfile)
                break
            elif A_max_ratio > 0.75 and A_max_ratio < 1.25:
                # The integer profile has obtained an area of 75% of the total area this profile can conain
                print("\n   EXTRA WARNING: integer profile reached 75% of its maximum atainable area!!!\n")
                self.NProfile.round(out=self.NProfile)
                break
            else:
                A_old_ratio = copy.copy(A_new_ratio)
                self.scale(ratio)

        print("\n--------->\n")
        print(f"WARNING: An integer value profile was created with an area ratio of {A_new_ratio}.\n")
        print("         Profile details might be lost and resulting SNR might differe from expectation...\n")
        print("--------->\n")

    def plot_profile(self,Psnr=None,outnm=None):
        plt.figure()
        ax_im = plt.axes((0.1, 0.1, 0.6, 0.6))
        time = np.arange(0,self.numspectra)*self.dt
        ax_im.set_xlabel("Time (s)")

        ax_im.imshow(self.NProfile, extent=(time[0], time[-1], self.freqs[-1], self.freqs[0]), aspect='auto')
        ax_im.set_ylabel("Frequency (MHz)")

        ax_sp = plt.axes((0.7, 0.1, 0.25, 0.6), sharey=ax_im)
        ax_sp.plot(np.sum(self.NProfile, axis=1)[::-1]/self.numspectra, np.flip(self.freqs), 'k-')
        plt.setp(ax_sp.get_yticklabels(), visible = False)

        ax_ts = plt.axes((0.1, 0.7, 0.6, 0.25), sharex=ax_im)
        timeseries=np.sum(self.NProfile, axis=0)
        popt = self.boxcar_pulse_para(TS=timeseries)
        ax_ts.plot(time, timeseries, 'k-')
        if np.argmax(timeseries) > popt[0]-popt[2]/2 and np.argmax(timeseries) < popt[0]+popt[2]/2:
            ax_ts.plot(time,self.boxcar(time, popt[0]*self.dt, popt[1], popt[2]*self.dt))
        else:
            ax_ts.plot(time,self.boxcar(time, self.ctr*self.dt, popt[1], self.width*self.dt))
        plt.setp(ax_ts.get_xticklabels(), visible = False)
    
        
        Ats = np.sum(timeseries)
        plt.figtext(0.75, 0.95, r"DM = %.3f cm$\mathrm{^{-3}}$pc" % self.dm, size='small')
        plt.figtext(0.75, 0.9, "Area = %.4f" % Ats, size='small')
        plt.figtext(0.75, 0.85, "Max value = %.4f" % np.max(self.NProfile), size='small')
        if Psnr !=None:
            plt.figtext(0.75, 0.8, "SNR       = %.3f" % Psnr, size='small')
        #plt.figtext(0.75, 0.75, "FWHM = %.6f" % (popt[2]*self.dt), size='small')
        plt.figtext(0.75, 0.75, "FWHM = %.6f" % (self.width*self.dt), size='small')

        if outnm != None:
            plt.savefig(outnm.split('.')[0]+".pdf")
        else:
            plt.show()

    def save_profile(self,name='SP_profile_'+jdutil.datetime.isoformat(jdutil.datetime.utcnow())+'.npy'):
        """
        Save the profile to a .npy file
        """
        split_name = name.split('.')
        if split_name[-1] != 'npy':
            split_name[-1] = 'npy'
            name = '.'.join(split_name)
        np.save(name,self.NProfile)
        print("\nProfile saved as %s\n" % name)


def down_convert(data,nbits,MIN,MAX):
    """
    Down converts N-bit data to lower resolution.
    To ensure that the dynamic range of the data fits
    into the output datatype dynamic range.

    Inputs:
        data:       2D numpy array
        nbits:      Number of bits of the output data
        MIN:        Minimum value of data set
        MAX:        MAximum value of data set

    Output:
        Down converted 2D numpy array with correct dtype
    """
    dtype = filterbank.get_dtype(nbits)
    if filterbank.is_float(nbits):
        error_msg = "\n"
        return data,error_msg

    else:
        tinfo = np.iinfo(dtype)
        dtype_dr  = 2.**nbits
        data_dr   = MAX-MIN
        global_scale = 1

        if data_dr > dtype_dr:
            global_scale *= dtype_dr/data_dr

            error_msg  = "\n--------->\n"
            error_msg += "WARNING: data has been scaled with a factor of %f.\n" % global_scale
            error_msg += "         This to ensure its dynamic range to fit in the dtype range.\n"
            error_msg += "         The fidelity of the data might be affected.\n"
            error_msg += "--------->\n"
        else:
            global_scale = 1. # Ensure no scaling is applied
            error_msg = "\n"

        # Ensure data range is within dtype range
        MAX *= global_scale
        if MAX > tinfo.max:
            shift = tinfo.max - MAX
        else:
            shift = 0.

        # Scale data
        return (data*global_scale + shift),error_msg


#######################################################
#
# Main program
#
#######################################################

def main():
    import fake_fil as ff

    ### Load base data
    if args.base_type == 'fil':
        base_params = [args.infil,args.PRMS,args.dm,args.Sfil,args.Pt]
    elif args.base_type == 'fake':
        base_params = [args.dt,args.fch1,args.chanbw,args.nchan,args.Ttot,args.MED,args.RMS]
        if args.nbits == None:
            args.nbits = 8
    else:
        raise ValueError("Can not deterime in what type of data SP must be injected...")

    base = ff.Base_data(args.outname,args.nbits,args.base_type,*base_params)

    if args.base_type == 'fil' and args.verbose:
        print(f"\nStatistics of base data time series for file {args.infil}:")
        print(f"   std  = {base.rms}")
        print(f"   skew = {base.skew}")
        print(f"   kurt = {base.kurt}")
        print(f"{base.rms} {base.skew} {base.kurt}")
    elif args.verbose:
        print(f"\nStatistics of base data time series for file {args.outname}:")
        print(f"   std  = {base.rms}")

    # Check if bit-rate of output should be the same as input
    if args.nbits == None:
        args.nbits = base.nbits

    # Check if channels are in correct frequency order
    if base.chan_bw >= 0.:
        raise ValueError("Input data channels are in the wrong frequency order!\nChannels should be ordered in decreasing frequency")

    ### Create the output filterbank file
    print('Creating file: %s' % args.outname)
    outfil = filterbank.create_filterbank_file(args.outname, base.header, nbits=args.nbits, mode='append')
    if args.nbits < base.nbits:
        print("\n--------->")
        print("WARNING: downconverting data to %i-bits, conversion effects might occur!" % args.nbits)
        print("--------->\n")

    ### Create SP profile
    print("\nCreating SP profile to inject.")
    prof = SP_Profile(outfil.freqs,dt=base.dt,dm=args.dm,profile=args.prof,Tprof=args.Tprof,Fprof=args.Fprof,amp2snr=args.amp2snr,abssnr=args.abssnr,verbose=args.verbose)
    if args.save_prof:
        # Parse the output filename to profile to save the profile with the same name
        args.save_prof = args.outname
    prof.build_burst(patchy=args.patchy,sp_idx=args.sp_idx,sp_run=args.sp_run, \
                     drift=args.drift,tilt=args.tilt,cleave=args.cleave,save_prof=args.save_prof)

    # Determine profile parameters independent of propagation effects
    if args.prop_ind:
        prof.boxcar_pulse_para()

    # Apply propagation efffects
    if args.verbose:
        print("  -> Applying propagation effects...")
    prof.propagate_burst(scint=args.scint,smear=args.smear,scatter=args.scatter,scat_dm=args.scat_dm)

    # Scale burst to desired SNR or flux
    if args.verbose:
        print("  -> Scaling burst to desired SNR or flux...")
    SNR = prof.amplitude_scaling(chan_bw=base.chan_bw,rms=base.rms,flux=args.flux,snr=args.snr)

    # If out fil uses integers, convert profile to integer values before injecting
    if not filterbank.is_float(args.nbits):
        if args.verbose:
            print("  -> Converting profile to integer value profile...")
        prof.int_prof(args.nbits)

    if args.plot_prof:
        prof.plot_profile(Psnr=(np.sum(SNR)/prof.numchans))

    if args.save_prof:
        # Additionaly save the full processed undispersed pulse profile
        prof.save_profile(name="undispersed_prof_"+args.outname)

    # Apply dispersion
    if args.verbose:
        print("  -> Dispersing the profile...")
    prof.disperse()

    ### Get read and write parameters
    # Determine from where to start reading data
    if args.base_type == 'fil':
        startbin = int(np.floor(args.Sfil/prof.dt))
    else:
        startbin = 0

    # Determine when to stop reading data
    if (args.base_type == 'fil') and (args.Tfil != None):
        endbin = int(np.ceil(args.Tfil/prof.dt)) + startbin
        if endbin > base.numspectra:
            endbin = base.numspectra
            print("\n--------->")
            print("WARNING: Only %0.3f seconds of data are going to be used." % ((endbin-startbin)*prof.dt))
            print("--------->\n")
    else:
        endbin = base.numspectra

    # Check base data compatability
    intsigbin = int(np.floor(args.Pt/base.dt)) + startbin # To inject SP Pt seconds into selected data window
    if intsigbin >= endbin:
        raise ValueError("Pulse injection time later then latest time bin in selected data!")
    if (int(np.ceil(prof.disperse(calc_only=True)))+intsigbin) > endbin:
        print("\n--------->")
        print("WARNING: DM sweep is longer than the duration of the selected data. Edge effects might be visible!")
        print("--------->\n")


    ### Loop over data and inject pulse
    # Give info on progress
    oldprogress = 0
    print("\nWriting data...")
    sys.stdout.write(" %3.0f %%\r" % oldprogress)
    sys.stdout.flush()

    # Start looping over data
    profstart = intsigbin - int(np.floor(prof.ctr))
    profend = profstart + prof.numspectra
    windowstart = startbin
    windowend = startbin + args.block_size

    while True:
        ## Determine if burst needs to be injected into data window and retrieve proper data format
        if not args.no_inject:
            ## Get profile spectra
            # Window within prof window
            if (windowstart > profstart) and (windowend < profend):
                spectra = base.load_spectra(windowstart, windowend-windowstart)

                toinject = prof.NProfile[:,windowstart-profstart:windowend-profstart]
                spectra = np.add(spectra, toinject, out=spectra)
            # Profile widow within window
            elif (windowstart < profstart) and (windowend > profend):
                spectra = base.load_spectra(windowstart, windowend-windowstart)

                append_left = np.append(np.zeros((prof.numchans,profstart-windowstart)),prof.NProfile,axis=1)
                append_right = np.append(append_left,np.zeros((prof.numchans,windowend-profend)),axis=1)
                toinject = append_right
                spectra = np.add(spectra, toinject, out=spectra)
            # Window on left prof boundaries
            elif (windowstart < profstart) and (windowend > profstart):
                spectra = base.load_spectra(windowstart, windowend-windowstart)

                toinject = np.append(np.zeros((prof.numchans,profstart-windowstart)),prof.NProfile[:,0:windowend-profstart],axis=1)
                spectra = np.add(spectra, toinject, out=spectra)
            # Window on right prof boundaries
            elif (windowstart < profend) and (windowend > profend):
                spectra = base.load_spectra(windowstart, windowend-windowstart)

                toinject = np.append(prof.NProfile[:,windowstart-profstart:prof.numspectra],np.zeros((prof.numchans,windowend-profend)),axis=1)
                spectra = np.add(spectra, toinject, out=spectra)
            else:
                spectra = base.load_raw_data(windowstart, windowend-windowstart)
        else:
            spectra = base.load_raw_data(windowstart, windowend-windowstart)

        ## Ensure spectra+SP fit into dynamic range dtype and down convert if necesarry
        #spectra,error_msg = down_convert(spectra,outfil.nbits,base.minimum,prof.max_val+(base.rms/np.sqrt(base.numchans))+base.med)

        ## Write spectra to file
        if len(spectra.shape) == 1:
            # Write raw data to fil
            outfil.filfile.seek(0, os.SEEK_END) # Move to end of file
            outfil.filfile.write(spectra.astype(outfil.dtype))
        else:
            # Append spectra to fil
            outfil.append_spectra(spectra.transpose())

        # Print progress to screen
        progress = int(100.0*windowend/endbin)
        if progress > oldprogress:
            sys.stdout.write(" %3.0f %%\r" % progress)
            sys.stdout.flush()
            oldprogress = progress

        # Prepare for next iteration
        windowstart += args.block_size
        windowend = min(windowstart+args.block_size, endbin)

        # Reached end of file?
        if windowstart > endbin:
            break

    outfil.close()
    sys.stdout.write("Done   \n\n")
    sys.stdout.flush()
    #print(error_msg)


    #NOTE: yet to implement:
    # - reversed frequency
    """
        #Check the order of the channels
        frequencies = fil.frequencies
        flip = False
        if frequencies[0] < frequencies[fil.nchans-1]:
            flip = True
        frequencies = np.flipud(frequencies)


                if flip:
                    injected = np.transpose(np.flipud(np.add(np.flipud(trans_spectra), toinject)))
                else:
                    injected = np.transpose(np.add(trans_spectra, toinject))

    """
    # - maybe obtain scat_dm automatically from NE2001?



if __name__ == '__main__':
    parser = argparse.ArgumentParser(prog='FRBfaker.py', description="v4.0 Leon Houben (July, 2021)", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    datenow = jdutil.datetime.utcnow()
    MJDnow  = str(datenow.to_mjd()).replace('.','_')

    # Define main parser arguments    
    parser.add_argument("-b", "--nbits", dest='nbits', type=int, default=None, \
                        help="Number of bits in the output Filterbank file. Defaults to same as input Filterbank, else 8-bit.")
    parser.add_argument("-o", "--outname", dest='outname', type=str, default='Injected-SP_'+MJDnow+'.fil', \
                        help="Name of output file.")
    parser.add_argument("--block-size", dest='block_size', type=int, default=10000, \
                        help="Number of spectra per read. This is the amount of data manipulated/written at a time.")
    parser.add_argument('--no_inject', action='store_true', default=False, \
                        help="Do NOT inject the pulse profile into the data and only print out data/pulse statistics and resulting data file.")
    parser.add_argument('--save_prof', action='store_true', default=False, \
                        help="Save the created (undispersed and unpropagated) SP profile to a numpy ndarray file (.npy)")
    parser.add_argument('--plot_prof', action='store_true', default=False, \
                        help="Interactivally plot the created (undispersed) SP profile.")
    parser.add_argument("-v", "--verbose", action='store_true', default=False, \
                        help="Print more operation details")


    # Define groups
    pulse = parser.add_argument_group('SP profile arguments')
    pulse_freq = pulse.add_mutually_exclusive_group()
    pulse.add_argument('--prof', default=False, metavar='<file name>.npy', \
                        help="Load custom numpy profile from <file name>.npy")
    pulse.add_argument('--Tprof', dest='Tprof', default=0.002, metavar='FWHM or ctr0,amp0,fwhm0, ... , ctrN,ampN,fwhmN', \
                        help="Specify shape of SP profile in the time domain. Either a single Gaussian with FWHM (in seconds) or multi-Gaus with params in a comma seperated list.")
    pulse_freq.add_argument('--Fprof', dest='Fprof', default=False, metavar='FWHM or ctr0,fwhm0, ... , ctrN,fwhmN', \
                        help="Specify shape of SP profile in the frequency domain. Either a single Gaussian with FWHM (in seconds) or multi-Gaus with params in a comma seperated list.")
    pulse.add_argument('--amp2snr', action='store_true', default=False, \
                        help="Indicates that the given amplitudes for --Tprof are in units of SNR. I.e. do not apply later SNR scaling.")
    pulse.add_argument('--abssnr', action='store_true', default=False, \
                        help="Given Tprof SNRs are absolute SNRs, i.e. take pulse overlaps into account. Otherwise subcomponents are just stacked on top of each other.")
    pulse.add_argument('-d', '--dm', type=float, default=0.0, \
                        help="Dispersion measure of the (fake) injected signal.")
    pulse.add_argument("-t", "--time", dest='Pt', type=float, default=0.0, \
                        help="Time at which the pulse should be injected in the highest frequency band from the start of the data set (in seconds).")
    pulse.add_argument('--sp_idx', nargs=2, type=float, default=[0.,None], metavar=('alpha', 'nu_ref'), \
            help="If given, flux (S) should be scaled according to S = S*(nu/nu_ref (in MHz))^alpha.")
    pulse_freq.add_argument('--sp_run', type=float, default=0, metavar='zeta', \
            help="Scale flux (S) using a spectral running: S = S*(nu/nu_ref (in MHz))^(alpha + zeta*(nu/nu_ref)).")
    pulse_freq.add_argument('--patchy', action='store_true', default=False, \
                        help="Create a random 'blob' of emmision. I.e. the pulse is only visible in a limited frequency range.")
    pulse.add_argument('--drift', nargs=2, default=False, metavar=('drift_rate', 'cav_widths'), \
                        help="Make a burst with subcomponents that drift down in frequency with a rate 'drift_rate' in MHz/s. The distance in seconds between each subcomponents peak is given in a comma-seperated list with cav_widths.")
    pulse.add_argument('--tilt', default=False, metavar='angle', \
                        help="Angle or comma seperated list of angles (in degrees) over which to tilt the individual subcomponents around their peak intensity.")
    pulse.add_argument('--cleave', default=False, metavar='FWHM or ctr0,fwhm0, ... , ctrN,fwhmN', \
                        help="Create a cavity of width FWHM at the centre of the pulse profile, or create several where the given ctrs are relative to the centre of the pulse profile. All values should be given in seconds and negative values are allowed (but do not start the list with a negative value!).")
    pulse.add_argument('--scint', nargs='*', default=False, metavar=('Ns','sc_bw'), \
                        help="Apply a random or regular (set sc_bw to 0) scintillation pattern to the burst's frequency structure. Compose the pattern of Ns sinusoids with a scintal bandwidth of sc_bw MHz. If no sc_bw is given it defaults to 1 - 2%% of the full BW.")
    pulse.add_argument('--smear', action='store_true', default=False, \
                        help="DM smear the SP profile.")
    pulse.add_argument('--scatter', nargs='*', default=False, metavar=('tau_scatt', 'nu_ref'), \
                        help="Scatter the SP profile with broadening function by Cordes and Lazio 2003 (Default) or a (nu/nu_ref)^-4.4 relation (give tau_scatt in seconds and nu_ref in MHz, if nu_ref not present you will be prompted to give nu_ref).")
    pulse.add_argument('--scat_dm', type=float, default=None, \
                        help="Dispersion measure to use for scattering. Defaults to same as DM of SP.")
    scale_arg = pulse.add_mutually_exclusive_group()
    scale_arg.add_argument("--snr", dest='snr', type=float, default=1., \
                        help="Scale profile to atain a pulse with given S/R.")
    scale_arg.add_argument("--flux", dest='flux', type=float, nargs=2, metavar=('flux', 'SEFD'), \
                        help="Scale profile to atain a pulse with given flux.")
    pulse.add_argument('--prop_ind', action='store_true', default=False, \
                        help="Scale profile independent of propagation effects. Smearing and scattering will affect the set flux or snr of profile.")


    # Define subparsers
    base_data = parser.add_subparsers(title='Inject into data type', dest='base_type')

    into_fil = base_data.add_parser('fil',help='Inject into fil file',formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    into_fil.add_argument('infil', type=str, \
                        help="Filename of Filterbank file in which SP must be injected.")
    into_fil.add_argument('-S', '--seek', dest='Sfil', type=float, default=0.0, \
                        help="Start reading Filterbank file from S seconds into file.")
    into_fil.add_argument('-T', '--total', dest='Tfil', type=float, \
                        help="Only process T amount of seconds.")
    into_fil.add_argument('--prestoRMS', dest='PRMS', action='store_true', default=False, \
                        help="Use 'prepdata' of PRESTO to determine RMS")

    into_fake = base_data.add_parser('fake',help='Inject into fake data',formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    into_fake.add_argument('--dt', type=float, default=54.613333e-6, \
                        help="Sampling time fake date (in seconds).")
    into_fake.add_argument('--fch1', type=float, default=1510.0, \
                        help="Freqiency of highest channel (in MHz).")
    into_fake.add_argument('--chanbw', type=float, default=0.5859375, \
                        help="-1 * Channel bandwidth (in MHz).")
    into_fake.add_argument('--nchan', type=int, default=512, \
                        help="Number of channels of fake data.")
    into_fake.add_argument('--Ttot', type=float, default=60.0, \
                        help="Total time, duration, of output fake data (in seconds).")
    into_fake.add_argument('--MED', type=float, default=None, \
                        help="Median value of fake data which defaults to centre of output data type's bit range.")
    into_fake.add_argument('--RMS', type=float, default=None, \
                        help="Standard diviation of fake data which defaults to 0.125 * 2^nbits")


    args = parser.parse_args()

    # Check if the profile options are given correctly
    profile_options = [args.Tprof]
    if args.Fprof:
        profile_options.append(args.Fprof)
    if args.cleave:
        profile_options.append(args.cleave)

    for num,prof_opt in enumerate(profile_options):
        try:
            prof_opt = float(prof_opt)
        except ValueError:
            prof_opt = prof_opt.split(',')
        finally:
            if num == 0:
                args.Tprof = prof_opt
            elif num == 1 and args.Fprof:
                args.Fprof = prof_opt
            else:
                args.cleave = prof_opt

    if args.sp_run != 0 and args.sp_idx[1] == None:
        parser.error("--sp_run requires one to set --sp_idx!")
    if hasattr(args, 'chanbw'):
        if args.chanbw > 0:
            args.chanbw *= -1. #To make channel BW negative value
    if args.Pt < 0.:
        parser.error("[-t Pt]   Time at which the pulse should be injected must be ABOVE zero.")
    if args.drift:
        if not args.Fprof and not args.sp_run and not args.patchy:
            parser.error("To make a downward drifting multi-component burst, the subcomponents need to be band limited! Set either of the three options {Fprof, sp_run, patchy} to achieve this.")
        else:
            drift = {}
            drift["drift_rate"] = float(args.drift[0])
            drift["cav_wths"] = args.drift[1].split(",")
            args.drift = drift
    if args.tilt:
        args.tilt = args.tilt.strip('[')
        args.tilt = args.tilt.strip(']')
        args.tilt = args.tilt.split(',')
    if args.scint != False:
        if args.scint == None:
            print("Applying a random scintillation pattern with a scintal bandwidth 1-2% of full bandwidth and 5 sinusoids")
            args.scint = [5, None]
        elif len(args.scint) == 1:
            if float(args.scint) == 0:
                parser.error("Also specify the number of scintals Ns the regular scintillation pattern should consist of with --scint!")
            elif args.scint.isdigit():
                print(f"Applying a random scintillation pattern with a scintal bandwidth 1-2% of full bandwidth and {args.scint} sinusoids")
                args.scint = [args.scint,None]
            else:
                print(f"Applying a random scintillation pattern with a scintal bandwidth of {args.scint} MHz and 5 sinusoids")
                args.scint = [5, args.scint]
        elif len(args.scint) > 2:
            parser.error("Only 1 or 2 arguments can be given to --scint!")
    if args.scatter != False:
        if args.scatter == None:
            print("Using broadening relation from Cordes and Lazio (2003) for the exponential scattering tail.")
            args.scatter = [None,None]
        elif len(args.scatter) == 1:
            print("Using a (nu/nu_ref)^-4.4 relation for the exponential scattering tail.")
            print("Give nu_ref in MHz, just hit <enter> to use the highest freq. of the output data: ")
            nu_ref = input()
            if nu_ref == '':
                nu_ref = None
            args.scatter = [args.scatter,nu_ref]
        elif len(args.scatter) == 2:
            print(f"Using a (nu/nu_ref)^-4.4 relation for the exponential scattering tail, with the given tau_scatt ({args.scatter[0]}) and nu_ref ({args.scatter[1]} MHz).")
        else:
            parser.error("Only 1 or 2 arguments can be given to --scatter!")

    main()
