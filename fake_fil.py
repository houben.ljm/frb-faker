#!/usr/bin/env python3

########################################################################################################################################
#
# This program loads an existing Filterbank file or creates a new one consisting of Gaussian noise
#
# Author:  Leon Houben
# Website: https://gitlab.com/houben.ljm
#
########################################################################################################################################

# Import modules
import os,sys
import argparse
import warnings
import matplotlib.pyplot as plt
import numpy as np
from scipy.stats import skew,kurtosis
from subprocess import Popen, PIPE
from presto import filterbank
import jdutil



class Base_data(object):
    """
    Makes a 2D numpy array with Gaussian noise to inject a SP in (Default) or
    loads a given Filterbank file.
    """
    def __init__(self,outname,nbits=8,base_type='fake',*data_params):
        """
        Base data constructor

        Inputs:
            nbits:       The number of bits in the output Filterbank file.
            outname:     The filename of the output Filterbank file.
            base_type:   ['fil'/'fake'] 'fil' to load input from existing Filterbank.
                                        'fake' to create Gaussian noise data. (Default)
            data_params: 'fil'  -> ['filename',PRMS,DM,Sfil,Pt]
                                   filename = input Filterbank file name.
                                   PRMS     = if True, determine rms of data using PRESTO. (Default: False)
                                   DM       = DM of burst to be injected. (Default: 0.0)
                                   Sfil     = Start extracting data Sfil seconds into original fil file. (Default: 0.0)
                                   Pt       = time (seconds into data) at which the pulse should be injected. (Default: 0.0)
                         'fake' -> [dt,fch1,chan_bw,nchan,Ttot,MED,RMS]
                                   dt      = sampling time fake date (in seconds). (Default: 54.613333e-6)
                                   fch1    = freqiency of highest channel (in MHz). (Default: 1510.0)
                                   chan_bw = negative! channel bandwidth (in MHz). (Default: -0.5859375)
                                   nchan   = number of channels of fake data. (Default: 512)
                                   Ttot    = Total time of output fake data (in seconds). (Default: 600.0)
                                   MED     = median value of fake data which defaults to centre of output data bit range.
                                   RMS     = standard diviation of fake data from MED. (Default: 0.125 * 2**nbits)

        Output:
            Base_data object
        """
        self.outname = outname
        if base_type == 'fil':
            assert len(data_params) == 5, "Incorrect number of arguments given"
            if not os.path.isfile(data_params[0]):
                raise IOError("%s does not exist!" % data_params[0])

            self.fil        = filterbank.FilterbankFile(data_params[0])
            self.dt         = self.fil.dt
            self.fch1       = self.fil.fch1
            self.chan_bw    = self.fil.foff
            self.numchans   = self.fil.nchan
            self.numspectra = self.fil.nspec
            self.nbits      = self.fil.nbits
            # Determine data statistics
            if int(np.floor(data_params[4]/self.dt)) >= self.numspectra:
                raise EOFError("Pulse injection time exceeds the duration of file %s!" % data_params[0])

            # Calculate dispersion sweep
            DisC = 4.1488e3          #Dispersion Constant
            Tdisperse = DisC * data_params[2] * (1.0/(self.fil.freqs**2) - 1.0/(self.fil.freqs[0]**2)) / self.dt #in bins

            # Extract spectra of base data in which to inject the fake burst
            Pspec = int(np.floor(data_params[3]/self.dt)) + int(np.floor(data_params[4]/self.dt))
            Pnoise_spectrum = self.load_spectra(max(Pspec-2**13,0),int(Tdisperse[-1])+2**14)

            # RMS value of data
            if data_params[1]:
                cmd  = "snr=$(prepdata -nobary -o inj_tmp -dm "+str(data_params[2])
                cmd += " "+data_params[0]+" | awk '/deviation/{print $4}')"
                cmd += " && rm -f inj_tmp.* && echo -n $snr"
                stdout, stderr = Popen([cmd], stdout=PIPE, shell=True).communicate()
                self.rms = float(stdout)
            else:
                # Calculate base data statistics after de-dispersing the noice spectrum
                # Apply de-dispersion to Pnoise_spectrum
                for idx,TS in enumerate(Pnoise_spectrum):
                    rollby = int(-1*np.round(Tdisperse[idx]))
                    Pnoise_spectrum[idx] = np.roll(TS,rollby)

                # Only select noise around time of injected burst
                Pnoise_spectrum = Pnoise_spectrum[:,:2**15]

                self.rms  = np.sum(Pnoise_spectrum,axis=0).std() # Get std of TS
                self.skew = skew(np.sum(Pnoise_spectrum,axis=0)) # Get skewness of TS
                self.kurt = kurtosis(np.sum(Pnoise_spectrum,axis=0)) # Get kurtosis of TS
            self.header = self.fil.header
            print('\nSP will be injected into: %s' % self.fil.filename)

        elif base_type == 'fake':
            assert len(data_params) == 7, "Incorrect number of arguments given"

            self.dt         = data_params[0]
            self.fch1       = data_params[1]
            self.chan_bw    = data_params[2]
            self.numchans   = data_params[3]
            self.numspectra = int(np.ceil(data_params[4]/self.dt))
            self.nbits      = nbits
            if data_params[5] == None:
                dtype = filterbank.get_dtype(nbits)
                if filterbank.is_float(nbits):
                    tinfo = np.finfo(dtype)
                else:
                    tinfo = np.iinfo(dtype)
                self.med = (tinfo.min + tinfo.max)/2
            else:
                self.med = data_params[5]
            if data_params[6] == None:
                self.channel_rms = 0.125*2**nbits
            else:
                self.channel_rms = data_params[6]
            self.rms        = self.channel_rms * np.sqrt(self.numchans)

            # Statistics for fake data
            fake_values     = np.random.normal(self.med,self.channel_rms,(self.numchans,int(4./self.dt)))
            self.minimum    = fake_values.min()
            self.maximum    = fake_values.max()
            self.make_header()
            print('\nSP will be injected into fake data')
        else:
            raise ValueError("Base_type should be either 'fil' or 'fake'.")

    def make_header(self):
        fil_header = {}
        fil_header["rawdatafile"] = self.outname
        fil_header["source_name"] = 'FAKE_data'
        fil_header["machine_id"] = 0
        fil_header["telescope_id"] = 0
        fil_header["src_raj"] = 0.0
        fil_header["src_dej"] = 0.0
        fil_header["az_start"] = 0.0
        fil_header["za_start"] = 0.0
        fil_header["data_type"] = 1 # filterbank
        fil_header["fch1"] = self.fch1
        fil_header["foff"] = self.chan_bw
        fil_header["nchans"] = self.numchans
        fil_header["nbeams"] = 1
        fil_header["ibeam"] = 0
        fil_header["nbits"] = self.nbits
        fil_header["tstart"] = jdutil.jd_to_mjd(jdutil.datetime_to_jd(jdutil.datetime.utcnow()))
        fil_header["tsamp"] = self.dt
        fil_header["nifs"] = 1

        self.header = fil_header

    def load_spectra(self,start,nspec):
        start_bin = np.round(start).astype('int')
        nspec_bin = np.round(nspec).astype('int')

        if hasattr(self, 'fil'):
            spectra = self.fil.get_spectra(start_bin,nspec_bin).data
        else:
            #Make fake data
            spectra = np.random.normal(self.med,self.channel_rms,(self.numchans,nspec_bin))

        return spectra

    def load_raw_data(self,start,nspec):
        start_bin = np.round(start).astype('int')
        nspec_bin = np.round(nspec).astype('int')

        if hasattr(self, 'fil'):
            # From PRESTO's get_spectra function:
            stop  = min(start_bin+nspec_bin, self.numspectra)
            nspec = stop - start_bin # account for possible end of file encounter
            num_to_read = nspec*self.numchans
            num_to_read = max(0, num_to_read)
            pos = self.fil.header_size+start*self.fil.bytes_per_spectrum
            self.fil.filfile.seek(pos, os.SEEK_SET)
            raw_data = np.fromfile(self.fil.filfile, dtype=self.fil.dtype, count=num_to_read)
        else:
            #Make fake data
            raw_data = np.random.normal(self.med,self.channel_rms,(nspec_bin,self.numchans)).flatten()

        return raw_data


def main():
    ### Load base data
    base_params = [args.dt,args.fch1,args.chanbw,args.nchan,args.Ttot,args.MED,args.RMS]
    base = Base_data(args.outname,args.nbits,'fake',*base_params)

    ### Create the output filterbank file
    print('\nCreating file: %s' % args.outname)
    outfil = filterbank.create_filterbank_file(args.outname, base.header, nbits=args.nbits, mode='append')

    ### Get read and write parameters
    startbin = 0
    endbin = base.numspectra
    block_size = 10000

    ### Loop over data and inject pulse
    # Give info on progress
    oldprogress = 0
    print("\nWriting data...")
    sys.stdout.write(" %3.0f %%\r" % oldprogress)
    sys.stdout.flush()

    # Start looping over data
    windowstart = startbin
    windowend = startbin + args.block_size

    while True:
        ## Get synthetic data spectra
        spectra = base.load_spectra(windowstart, windowend-windowstart)

        ## Write spectra to file
        outfil.append_spectra(spectra.transpose())

        # Print progress to screen
        progress = int(100.0*windowend/endbin)
        if progress > oldprogress:
            sys.stdout.write(" %3.0f %%\r" % progress)
            sys.stdout.flush()
            oldprogress = progress

        # Prepare for next iteration
        windowstart += args.block_size
        windowend = min(windowstart+args.block_size, endbin)

        # Reached end of file?
        if windowstart > endbin:
            break

    outfil.close()
    sys.stdout.write("Done   \n\n")
    sys.stdout.flush()

    return

if __name__ == '__main__':
    parser = argparse.ArgumentParser(prog='fake_fil.py', description="Make synthetic Filterbank data with Gaussian distributed noise. v1.0 Leon Houben (July, 2021)", \
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    datenow = jdutil.datetime.utcnow()
    MJDnow  = str(datenow.to_mjd()).replace('.','_')

    # Define main parser arguments
    parser.add_argument("--nbits", type=int, default=8, \
                        help="Number of bits in the output Filterbank file.")
    parser.add_argument('--dt', type=float, default=54.613333e-6, \
                        help="Sampling time fake of date (in seconds).")
    parser.add_argument('--fch1', type=float, default=1510.0, \
                        help="Frequency of highest channel (in MHz).")
    parser.add_argument('--chanbw', type=float, default=0.5859375, \
                        help="-1 * Channel bandwidth (in MHz).")
    parser.add_argument('--nchan', type=int, default=512, \
                        help="Number of channels of fake data.")
    parser.add_argument('--Ttot', type=float, default=60.0, \
                        help="Total time, duration, of output fake data (in seconds).")
    parser.add_argument('--MED', type=float, default=None, \
                        help="Median value of fake data which defaults to centre of output data type's bit range.")
    parser.add_argument('--RMS', type=float, default=None, \
                        help="Standard diviation of fake data which defaults to 0.125 * 2^nbits")
    parser.add_argument("-o", "--outname", dest='outname', type=str, default='FAKE_'+MJDnow+'.fil', \
                        help="Name of output file.")
    parser.add_argument("--block-size", type=int, default=10000, \
                        help="Number of spectra to write to file at a time.")

    args = parser.parse_args()
    if args.chanbw > 0:
        args.chanbw *= -1. #To make channel BW negative value

    main()
