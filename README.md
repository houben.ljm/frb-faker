# Fast Radio Burst Faker

Or actually just a Single Pulse (SP) faker...
This script enables you to inject a synthetic SP in pulsar data. You can set several options to alter the pulse shape, strength and location in the data file. SPs can also be injected in fully synthetic data made by the script itself.

Note 1: this script can only handle Filterbank files.\
Note 2: the best results are obtained when SPs are injected in floating point data. Otherwise (important) pulse details might be lost.

## Example
![A synthetic burst mimicking one from FRB121102. Which is which?](/images/Burst_comparison.jpg "An FRB121102 repeat burst! :D")

Two bursts from FRB121102.\
a) a fake burst injected in synthetic Effelsberg data created to match:\
b) a true repeat burst from FRB 121102 published by [Houben et al.](https://www.aanda.org/articles/aa/full_html/2019/03/aa33875-18/aa33875-18.html)

Things to take into consideration when comparing the two panels:
- Burst `a)` is injected in fully _synthetic data_, data consisting of pure Gaussian noise. The noise of true data, `panel b)`, is differently distributed due to RFI and receiver properties. This causes the burst in `panel a)` to look more smooth or vise versa, the burst in `panel b)` to have a more 'jittery' emmision patch.
- Receiver bandpasses are never truly flat and often show a strong roll-off at the band's edges. This can be seen clearly at the top of `panel b)`. It is not apparent for `panel a)`, since the synthetic data has a perfectly flat bandpass.
- RFI can cause channels to be over or under saturated. In `panel b)` this is true for channels around 1425 MHz that looks like the burst's emmission is a bit pinched off there. Yet another feature that is not present in synthetic data.\
So, to re-create bursts affected by the same receiver limitations as real detected bursts, they should be injected in real data!

[Go to 'Run a test'](https://gitlab.com/houben.ljm/frb-faker/-/blob/master/README.md#run-a-test), to see how the synthetic burst was created (`panel a)`).

## Getting Started
### Prerequisites
`FRBfaker.py` depends on some of the python libraries of [Scott Ransom's `PRESTO` version 3.0 or higher](https://github.com/scottransom/presto). Check the readme of `PRESTO` for detailed installation instructions.\
Furthermore, does `FRBfaker.py` from v3.0 and onwards use `python3` with the modules `matplotlib`, `numpy` and `scipy`, which might not be standardly installed with your python install.\
Lastly, does the script need `jdutil.py`, which is provided in this repository and was taken from the [ExifOTIO repository of Matt Davis](https://github.com/meshula/exifotio/blob/master/jdutil.py).

### Installing
Once all the above dependencies are installed on your machine and the appropriate paths are set in your .bashrc you can just run the script in its directory as `./FRBfaker.py`. Alternatively, you could add the path to the directory holding this README to your .bashrc's `PATH`. Then you can run the code everywhere
on your machine by just typing its name. Go inject some SPs!

## Usage
### FRBfaker.py
```
FRBfaker.py [-h] [-b NBITS] [-o OUTNAME] [--block-size BLOCK_SIZE]
            [--save_prof] [--plot_prof] [-v] [--prof <file name>.npy]
            [--Tprof FWHM or ctr0,amp0,fwhm0, ... , ctrN,ampN,fwhmN]
            [--Fprof FWHM or ctr0,fwhm0, ... , ctrN,fwhmN]
            [-d DM] [-t PT] [--sp_idx alpha nu_ref] [--sp_run zeta]
            [--patchy] [--drift drift_rate cav_widths] [--tilt angle]
            [--cleave FWHM or ctr0,fwhm0, ... , ctrN,fwhmN] [--scint [Ns, sc_bw]]
            [--smear] [--scatter [tau_scatt]] [--scat_dm SCAT_DM]
            [--snr SNR | --flux flux SEFD] [--prop_ind]
            {fil,fake} ...
```
Lots of options, so lets give a detailed description on all these options below.

### General arguments
*  **```-h, --help```**\
   Show the help message detailing the options.
*  **```-b NBITS, --nbits NBITS```** *(default: None)*\
   Sets the number of bits of the output Filterbank file. Defaults to same as input Filterbank, else 8-bit.
*  **```-o OUTNAME, --outname OUTNAME```** *(default: Injected-SP_<MJD now>.fil)*\
   Name of the output data file.
*  **```--block-size BLOCK_SIZE```** *(default: 10000)*\
   Sets the number of spectra processed at once.
*  **`--no_inject`** *(default: False)*\
   Do not inject the created pulse profile, to retrieve data/pulse statistics and a base data file.
*  **`--save_prof`** *(default: False)*\
   If set, the created SP profile is saved into a numpy ndarray (.npy) file. 
*  **`--plot_prof`** *(default: False)*\
   If set, the created undispersed SP profile is shown in an interactive plot.
*  **```-v, --verbose```** *(default: False)*\
   Show more operation details. 

### SP profile arguments
*  **`--prof <file name>.npy`** *(default: False)*\
   Inject a custom profile. Give the numpy array that contains the profile.\
   Note: the number of channels of the custom profile need to be the same as the intended output file!
*  **`--Tprof FWHM or ctr0,amp0,fwhm0, ... , ctrN,ampN,fwhmN`** *(default: 0.002)*\
   Specify the shape of the synthetic SP profile in the _time domain_. Either a simple single Gaussian with a FWHM (in seconds) is injected or multiple Gaussians are injected with their parameters (`ctr`, `amp`, `fwhm`) given in a comma seperated list.
*  **`--Fprof FWHM or ctr0,fwhm0, ... , ctrN,fwhmN`** *(default: False)*\
   Specify the shape of the synthetic SP profile in the _frequency domain_. The profile's frequency structure is modulated either with a simple single Gaussian with a FWHM (in MHz) or with multiple Gaussians with their parameters (`ctr`, `fwhm`) given in a comma seperated list.
*  **`-d DM, --dm DM`** *(default: 0.0)*\
   Sets the dispersion measure (DM) at which the SP should be injected in the data.
*  **`-t PT, --time PT`** *(default: 0.0)*\
   Set, after how many **seconds** of the start of the data file, the SP should be injected.\
   Note: this is the time at which the SP will show after dedispersion relative to the highest frequency channel.
*  **`--sp_idx alpha nu_ref`** *(default: [0.0, None])*\
   Scale the flux per frequency (S_f) according to `S_f = S_f*(nu/nu_ref)^alpha`. Give `nu_ref` **in MHz**.
*  **`--sp_run`** *(default: 0.)*\
   Scale flux (S_f) using a spectral running: `S_f = S_f*(nu/nu_ref (in MHz))^(alpha + zeta*(nu/nu_ref))`. In order to use a spectral running, the spectral index above should also be given!
*  **`--patchy`** *(default: False)*\
   If set, the SP's frequency spectrum is limited to a random frequency range. This will show as a 'blob' in its dynamic spectrum.
* **`--drift drift_rate cav_widths`** *(default: False)*\
   Make a burst with subcomponents that drift down in frequency with a rate `drift_rate` in MHz/s. The distance (in seconds) between each subcomponent's peak is given in a comma-seperated list with `cav_widths`.
* **`--tilt angle`** *(default: False)*\
   Angle or comma seperated list of angles (in degrees) over which to tilt the individual subcomponents around their peak intensity.
* **`--cleave FWHM or ctr0,fwhm0, ... , ctrN,fwhmN`** *(default: False)*\
   Create a cavity of width FWHM at the centre of the pulse profile, or create several, where the given ctrs are relative to the centre of the pulse profile. All values should be given in seconds and negative values are allowed (but do not start the list with a negative value!).
* **`--scint [Ns, sc_bw]`** *(default: False)*\
   Apply a scintillation pattern to the burst's frequency structure.
   Compose the pattern of `Ns` random sinusoids with a scintal bandwidth of `sc_bw` MHz. If no `sc_bw` is given it defaults to 1 - 2% of the full BW.
   If `sc_bw` is set to 0 the pattern is composed of a single sinusoid with `Ns` maxima.
*  **`--smear`** *(default: False)*\
   Convolves each channel with a boxcar to create the effect of DM smearing in a finit freq channel.
*  **`--scatter [tau_scatt]`** *(default: False)*\
   Convolve the SP profile with an exponential scattering tail calculated either with the broadening function of Cordes and Lazio (2003) or a `(nu/nu_ref)^-4.4` relation. If no value is given for `tau_scatt` the broadening function of Cordes and Lazio is used. Otherwise the user is prompted and asked to give a value for `nu_ref`.
*  **`--scat_dm SCAT_DM`** *(default: None)*\
   In case you want to inject an 'over' or 'under' scattered profile (in relation to the profile's DM), you can set a different DM to be used for scattering. If `scat_dm` is None, scattering will be applied for the `DM` of the SP profile.
*  **`--snr SNR`** *or* **`--flux flux SEFD`**
   - **`--snr SNR`** *(default: 1.0)*\
     Inject a SP with an SNR of `SNR`.
   - **`--flux flux SEFD`** *(default: None)*\
     Inject a SP with a flux of `flux`. Also the 'System Equivalent Flux Density' `SEFD` of the data's corresponding receiver must be given.

   **Note**: the synthetic SP will be scaled with an amplitude based on the given SNR or flux and assuming an injection background of pure Gaussian noise! I.e. if your data contains noise differently distributed, due to RFI, bandpass irregularites, ect. the injected burst will not have exactly the same SNR as specified.
*  **`--prop_ind`** *(default: False)*\
   Scale the SP's profile independent of propagation effects, so smearing and scattering will affect the snr or flux set above.

### Data arguments
#### fil
```
FRBfaker.py fil [-h] [-S SFIL] [-T TFIL] [--prestoRMS] infil
```
**`infil`** File name of Filterbank in which the SP must be injected.

*  **`-h, --help`** Show the help message detailing the options.
*  **`-S SFIL, --seek SFIL`** *(default: 0.0)*\
   Start reading Filterbank file from `Sfil` **seconds** into file.
*  **`-T TFIL, --total TFIL`** *(default: None)*\
   Only process `Tfil` amount of seconds. If None, then script will process until the end of the given Filterbank file.
*  **`--prestoRMS`** *(default: False)*\
   Use 'prepdata' of PRESTO to determine RMS of data.

#### fake
```
FRBfaker.py fake [-h] [--dt DT] [--fch1 FCH1] [--chanbw CHANBW]
                 [--nchan NCHAN] [--Ttot TTOT] [--MED MED] [--RMS RMS]
```
*  **`-h, --help`** Show the help message detailing the options.
*  **`--dt DT`** *(default: 5.4613333e-05)*\
   Sampling time of fake date **in seconds**.
*  **`--fch1 FCH1`** *(default: 1510.0)*\
   Frequency of highest channel **in MHz**.
*  **`--chanbw CHANBW`** *(default: 0.5859375)*\
   -1 * Channel bandwidth **in MHz**, i.e. give a positive value which will be multiplied by -1 by the script.
*  **`--nchan NCHAN`** *(default: 512)*\
   Number of channels of fake data.
*  **`--Ttot TTOT`** *(default: 60.0)*\
   Total time, duration, of output fake data **in seconds**.
*  **`--MED MED`** *(default: None)*\
   Median value of fake data which defaults to the centre of the output data type's bit range.
*  **`--RMS RMS`** *(default: None)*\
   Standard deviation of fake data which defaults to `0.125*2^nbits`.

### fake_fil.py
```
fake_fil.py [-h] [--nbits NBITS] [--dt DT] [--fch1 FCH1] 
            [--chanbw CHANBW] [--nchan NCHAN] [--Ttot TTOT]
            [--MED MED] [--RMS RMS]
            [-o OUTNAME] [--block-size BLOCK_SIZE]
```
Makes synthetic Filterbank data with Gaussian distributed noise.\
The default values are taken such that `fake_fil.py` will create synthetic data resembling that from the 100 meter Effelsberg radio telescope.

#### Optional arguments
*  **`-h, --help`**\
   Show the help message detailing the options.
*  **`--nbits NBITS`** *(default: 8)*\
   Number of bits of the output Filterbank file.
*  **`--dt DT`** *(default: 5.4613333e-05)*\
   Sampling time of fake date (in seconds).
*  **`--fch1 FCH1`** *(default: 1510.0)*\
   Frequency of highest channel (in MHz).
*  **`--chanbw CHANBW`** *(default: 0.5859375)*\
   -1 * Channel bandwidth (in MHz), i.e. give a positive value which will be multiplied by -1 by the script.
*  **`--nchan NCHAN`** *(default: 512)*\
   Number of channels of fake data.
*  **`--Ttot TTOT`** *(default: 60.0)*\
   Total time, duration, of output fake data (in seconds).
*  **`--MED MED`** *(default: None)*\
   Median value of fake data which defaults to the centre of the output data type's bit range.
*  **`--RMS RMS`** *(default: None)*\
   Standard deviation of fake data which defaults to `0.125*2^nbits`.
*  **```-o OUTNAME, --outname OUTNAME```** *(default: FAKE_<MJD now>.fil)*\
   Name of the output Filterbank file.
*  **```--block-size BLOCK_SIZE```** *(default: 10000)*\
   Sets the number of spectra to write to file at a time.

## Run a test
To see if the script works we are going to re-create [the synthetic burst shown above.](https://gitlab.com/houben.ljm/frb-faker/-/blob/master/README.md#example)\
FRB bursts have a variaty of complex morphologies which can be mimicked with the options of `FRBfaker.py`. To re-create a detected burst, it is crucial to have a good understanding about how a burst is build up from 2D Gaussians.

In order to properly re-make FRB 121102's burst, 2D Gaussians were fitted to the emmision in its dynamic spectrum. Through subtracting the fit and inspecting the residuals, an estimate was obtained about the quality of each fit. From these fits it became apparant that the burst consists of 2 Gaussians, tilted over an angle of 8 degrees and located a bit appart in both time and frequency. Taking the the integrated detection SNR from [Houben et al.](https://www.aanda.org/articles/aa/full_html/2019/03/aa33875-18/aa33875-18.html), we can now simply re-create the burst by injecting it in synthetic Effelsberg data (the radio telescope with which the burst was detected).
```
./FRBfaker.py --plot_prof -o test_pulse.fil --Tprof 0.039186,1,0.002790,0.043257,1,0.004 --Fprof 1463.075,180,1419.358,250 -d 560 -t 1 --tilt 8 --snr 28 fake --Ttot 5
```
Note: you will be prompted with the integer pulse profile that is going to be injected in synthetic data. Data that is created using all default values of `fake_fil.py`, hence the only option `--Ttot` to limit the amount of data that is going to be created.

If all went well, you should now have a Filterbank file `test_pulse.fil` containing the just created burst of FRB121102. To see if that is the case use `PRESTO`'s `waterfaller.py`:
```
waterfaller.py --show-ts --show-spec -T 0.9 -t 0.2 -d 560 --downsamp=16 --nsub=32 test_pulse.fil
```

You should now clearly see the two peaks of the re-created FRB121102 burst. If not, re-make the burst and plot it again. It might happen that the synthetic data contains low emmision exactly at the location of one of the two peaks by chanche. Re-making the burst should reveal both peaks.\
Since `waterfaller.py` is not used to make the plot shown in `panel b)` above, your burst will look slightly different from the one shown in the example section.

In this example we only used 3 of `FRBfaker.py`'s options to massage the shape of a pulse profile to inject. There are many more to play around with, which can be used to make much more complex structures and shapes. Enjoy!

## Finally
### Author
*  [**Leon Houben**](https://www.linkedin.com/in/houbenljm) - Radboud University / Max Planck for Radio Astronomy

### Acknowledgements
Big thanks to Guðjón Henning Hilmarsson for playing around with the code and providing useful operation feedback!
